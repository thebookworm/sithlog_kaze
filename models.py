__author__ = 'leonardo'

import datetime

try:
    from mongoengine import *
    try:
        connect('sithlog')
    except IOError:
        print("ERROR: mongoengine could not connect to sithlog document db.")
except ImportError:
    print("ERROR: mongoengine module could not be found in PYTHONPATH.")

class Article(Document):
    """
    Simple Article document db
    author, title and content are required
    publish_date is set automatically
    """
    author = StringField(required=True)
    title = StringField(required=True)
    content = StringField(required=True)
    publish_date = DateTimeField(default=datetime.datetime.now)