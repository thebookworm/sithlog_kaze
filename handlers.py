#!/bin/env python
__author__ = 'Leonardo Pepe de Freitas'

import tornado.web
import tornado.auth
from models import *

class BasicHandler(tornado.web.RequestHandler):
    """
    The BasicHandler object implements a default current user identification
    and a default template render
    """
    def validate_user_admin(self):
        user_name = self.get_secure_cookie('user')
        if user_name == '"https://www.google.com/accounts/o8/id?id=AItOawmCqNBWp1Oqad48SvZXWlehh91qI25QPNw"':
            self._user_admin=user_name
        else:
            raise tornado.web.HTTPError(403, 'User not authorized')

    def get_current_user(self):
        return self.get_secure_cookie("user")

    def template_render(self, template_name, **kwargs):
        """
        The tempalte_render() method receives the template_name kwargs
        and return the content rendered by the predefined HTML template
        """
        return tornado.web.RequestHandler.render(self, template_name, **kwargs)

class GoogleAuthHandler(BasicHandler, tornado.auth.GoogleMixin):
    """
    GoogleAuthHandler to check if the current_user has admin rights
    """
    @tornado.web.asynchronous
    def get(self):
        if self.get_argument('openid.mode', None):
            self.get_authenticated_user(self.async_callback(self._on_auth))
            return

        ## redirect after auth
        self.authenticate_redirect()

    def _on_auth(self, user):
        ## auth fail
        if not user:
            raise tornado.web.HTTPError(500, 'Google auth failed')

        ## auth success
        identity = self.get_argument('openid.identity', None)

        ## set identity in cookie
        self.set_secure_cookie('user', tornado.escape.json_encode(identity))
        self.redirect('/')

class HomeHandler(BasicHandler):
    def get(self):
        posts = Article.objects()
        return self.render(template_name='templates/blog_post.html', posts=posts)

class PublishArticle(BasicHandler):
    """
    This Object implements a RESTful interface

    GET method will return all the posts stored on the database document
    POST method will enable the admin user to insert articles on the database document
    """
    def get(self):
        """
        Render the post_form.html for visualization
        """
        self.validate_user_admin()
        return self.render(template_name='templates/post_form.html')

    def post(self, **kw):
        self.validate_user_admin()
        author = self.get_argument('author')
        title =self.get_argument('title')
        content = self.get_argument('content')
        post = Article(author=author, title=title, content=content)
        post.save()
        self.redirect('/')